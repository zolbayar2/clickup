const axios = require("axios");

async function run() {
  const spaceId = "90070114754";
  const response = await axios.get(
    `https://api.clickup.com/api/v2/space/${spaceId}/list`,
    {
      headers: {
        Authorization: "pk_49004962_YWQKVQTO0K4UX6G95UOWUXNC5WGGEB01",
      },
    }
  );

  const data = response;
  console.log(data);
}

run();
